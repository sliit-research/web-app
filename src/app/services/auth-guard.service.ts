import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService } from '../auth/services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(public authService: NbAuthService, private router: Router) {}

  canActivate() {

    this.authService.isAuthenticated().subscribe(res => {
       if (!res) {
         this.router.navigate(['/auth/login']);
       }
    });
    return this.authService.isAuthenticated();
  }

}
