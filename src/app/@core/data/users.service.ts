import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

let counter = 0;

@Injectable()
export class UserService {

  private users = {
    gayan: { name: 'Gayan Kalhara', picture: 'assets/images/gayan.png' },
    achala: { name: 'Achala Dias', picture: 'assets/images/gayan.png' },
    vishan: { name: 'Vishan Danura', picture: 'assets/images/gayan.png' },
    vishva: { name: 'Vishva Ratnayake', picture: 'assets/images/gayan.png' },
  };

  private userArray: any[];

  constructor() {
    // this.userArray = Object.values(this.users);
  }

  getUsers(): Observable<any> {
    return Observable.of(this.users);
  }

  getUserArray(): Observable<any[]> {
    return Observable.of(this.userArray);
  }

  getUser(): Observable<any> {
    counter = (counter + 1) % this.userArray.length;
    return Observable.of(this.userArray[counter]);
  }
}
