import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ModalComponent } from './modals/modal/modal.component';

const components = [
  ModalComponent,
];

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    ...components,
  ],
  entryComponents: [
    ModalComponent,
  ],
})
export class UiFeaturesModule { }
