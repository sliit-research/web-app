import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Manage Devices',
    icon: 'nb-home',
    children: [
      {
        title: 'Sound Processors',
        link: '/',
      },
      {
        title: 'Listening Posts',
        link: '/',
      },
      {
        title: 'Base Stations',
        link: '/',
      },
    ],
  },
  {
    title: 'Support',
    group: true,
  },
  {
    title: 'Report a Bug',
    icon: 'nb-home',
    link: '/pages/dashboard',
  },
  {
    title: 'Customer Support',
    icon: 'nb-home',
    link: '/pages/dashboard',
  },
];
