/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import * as io from 'socket.io-client';
import { AnalyticsService } from './@core/utils/analytics.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './pages/ui-features/modals/modal/modal.component';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  private url = 'http://localhost:4380';
  private socket;
  constructor(private analytics: AnalyticsService, private modalService: NgbModal) {
    this.socket = io(this.url);
    this.socket.on('alert', (data) => {
      this.showStaticModal();
    });
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
  }

  showStaticModal() {
    const activeModal = this.modalService.open(ModalComponent, {
      size: 'sm',
      backdrop: 'static',
      container: 'nb-layout',
    });

    activeModal.componentInstance.modalHeader = 'Static modal';
    activeModal.componentInstance.modalContent = `This is static modal, backdrop click
                                                    will not close it. Click × or confirmation button to close modal.`;
  }
}
